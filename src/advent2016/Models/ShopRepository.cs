﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace advent2016.Models
{
	public class ShopRepository : IShopRepository
	{
		private static ConcurrentDictionary<string, Shop> _shops =
			  new ConcurrentDictionary<string, Shop>();
		
		public ShopRepository()
		{
			Add(new Shop { 
				Name = "Bento", 
				ZipCode = "000-0000", 
				Address = "Hakata", 
				PhoneNumber = "092-000-0000" 
			});
			Add(new Shop
			{
				Name = "Pizza",
				ZipCode = "111-1111",
				Address = "Tenjin",
				PhoneNumber = "092-111-1111"
			});
			Add(new Shop
			{
				Name = "Udon",
				ZipCode = "222-2222",
				Address = "Nakasu",
				PhoneNumber = "092-222-2222"
			});
			Add(new Shop
			{
				Name = "Ramen",
				ZipCode = "333-3333",
				Address = "Nagahama",
				PhoneNumber = "092-333-3333"
			});
		}

		public IEnumerable<Shop> GetAll() => _shops.Values;

		public void Add(Shop shop)
		{
			shop.Id = Guid.NewGuid().ToString();
			_shops[shop.Id] = shop;
		}

		public Shop Find(string id)
		{
			var shop = default(Shop);
			_shops.TryGetValue(id, out shop);
			return shop;
		}

		public void Remove(string id)
		{
			var shop = default(Shop);
			_shops.TryRemove(id, out shop);
		}

		public void Update(Shop shop) => _shops[shop.Id] = shop;
	}
}
