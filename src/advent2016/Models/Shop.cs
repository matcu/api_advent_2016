﻿using System;

namespace advent2016.Models
{
	public class Shop
	{
		public String Id { get; set; }
		public String Name { get; set; }
		public String ZipCode { get; set; }
		public String Address { get; set; }
		public String PhoneNumber { get; set; }
		public String Memo { get; set; }
	}
}
