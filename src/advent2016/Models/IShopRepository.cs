﻿using System.Collections.Generic;

namespace advent2016.Models
{
	public interface IShopRepository
	{
		void Add(Shop shop);
		IEnumerable<Shop> GetAll();
		Shop Find(string id);
		void Remove(string id);
		void Update(Shop shop);
	}
}
