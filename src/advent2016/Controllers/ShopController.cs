﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using advent2016.Models;

namespace advent2016.Controllers
{
	[Route("api/v1/[controller]")]
	public class ShopController : Controller
	{
		private IShopRepository Shops { get; set; }

		public ShopController(IShopRepository shops)
		{
			Shops = shops;
		}

		// GET api/v1/shop
		[HttpGet]
		public IEnumerable<Shop> Get() => Shops.GetAll();

		// GET api/v1/shop/{id}
		[HttpGet("{id}", Name = "GetShop")]
		public IActionResult Get(string id)
		{
			var shop = Shops.Find(id);

			if (shop == null)
				return NotFound();

			return new ObjectResult(shop);
		}

		// POST api/v1/shop
		[HttpPost]
		public IActionResult Post([FromBody]Shop shop)
		{
			if (shop == null)
				return BadRequest();

			Shops.Add(shop);

			return new CreatedResult("GetShop", shop);
		}

		// PUT api/v1/shop/{id}
		[HttpPut("{id}")]
		public IActionResult Put(string id, [FromBody]Shop shop)
		{
			if (shop == null || shop.Id != id)
				return BadRequest();

			if (Shops.Find(id) == null)
				return NotFound();

			Shops.Update(shop);

			return new NoContentResult();
		}

		// DELETE api/v1/shop/{id}
		[HttpDelete("{id}")]
		public void Delete(string id) => Shops.Remove(id);
	}
}
